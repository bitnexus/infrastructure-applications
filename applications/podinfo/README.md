# Podinfo

## Synopsis

This Kustomize project defines the configuration for a deployment of `podinfo` to a Kubernetes cluster

## Prerequisites

* kubectl
* kustomize
* A running Kubernetes cluster

## Getting Started

1. Install the distribution
  * From the `base/` directory run `kubectl apply -k .`

## Reference

### Files and Directories

1. `base/`
   * The root kustomazation configuration and k8s resources with default settings
     * `deployment.yaml` - The manifest for the `podinfo` deployment.
     * `hpa.yaml` - The manifest for configuring the horizontal pod autoscaler for `podinfo`
     * `ingress.yaml` - Configures an ingress resource for exposing `podinfo`. Will create a load balancer and associate a public IP address with it for access from outside the cluster.
     * `kustomization.yaml` - Lists the Kubernetes resources contained in the project
     * `service.yaml` - The manifest for the `podinfo` service. Exposes the `podinfo` deployment within the cluster. 

### Thoughts and Improvements

* Due to time constraints and less(*) experience with GCP I'm cutting a few corners, but can expand on them as necessary
  * *None, actually. If this is a deal breaker I get it, but it was fun getting a whirlwind intro either way
  * For that matter, I've never used kustomize before either. I'm comparing it to helm, which I am familiar with, and like it a lot better for simpler deployments. I was iffy about overrides using yaml, which I guess is the main idea, but I guess it's much neater than template-based resource definitions for most cases. Keeping track of separate values files with duplicate keys isn't the cleanest thing either.
* I'd normally configure host-based access and TLS for the ingress resource and `external-dns` to create DNS records for them.
* I'm not going to bother with overlays for now, but this is where you'd configure overrides or additional options for eg. environment-based configurations. A `dev` overlay might set `replicas: 1` for a `developmnt` environment vs `replicas: 12` for `production`, for example. 