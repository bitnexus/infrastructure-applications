# Infrastructure

## Synopsis

This terraform project automates the creation of a Google Kubernetes Engine (GKE) cluster and associated resources. It creates a custom VPC network and subnets, and creates a GKE cluster in the specified region.

## Prerequisites

* Terraform (minimum version 1.9.0)
* A GCS bucket for remote terraform state
* A locally-authenticated Google Cloud account
* A Google Cloud project ID and appropriate permissions to manage resources
* Enabled APIs
  * Compute
  * Google Kubernetes Engine

## Getting Started

1. Clone this repository
   ```
     git clone git@gitlab.com/bitnexus/infrastructure-applications
     cd infrastructure-applications
   ```
2. Initialize terraform
   * `terraform init`
3. Update tfvars file with desired values
   * Required:
      1. cluster_name: The name of the cluster
      2. organization_id: Your Google Cloud organization ID
      3. project_id: The ID of the project you wish to use for the cluster
    * Look at variables.tf for optional arguments and their default values
4. Create the cluster
   1. `terraform apply`
   2. Review the generated plan, and enter `yes` to apply the changes
5. Get cluster credentials and update your `~/.kube/config`
   * `gcloud container clusters --region <region> get-credentials cluster`

## Reference

### Terraform Files

1. backend.tf
   * Configures terraform to use remote state, and store it in the specified GCS bucket
2. cluster.tf
   * Configures the kubernetes cluster using Google's `terraform-google-kubernetes-engine` module
   * Enables the `kubernetes` terraform provider, deriving its values from the module's outputs
3. network.tf
   * Creates a VPC and network for the cluster and associated resources
   * Creates secondary IP ranges for the pod and service networks
4. outputs.tf
   * Configures outputs to expose information about the cluster
5. variables.tf
   * Configures variables necessary to initialize the project
6. versions.tf
   * Configures the minimum required terraform provider, as well as the required providers and their minimum versions.