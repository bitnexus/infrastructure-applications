terraform {
  backend "gcs" {
    bucket  = "bitnexus-tf-state"
    prefix  = "terraform/state"
  }
}