variable "cluster_name" {
  description = "The cluster name"
}

variable "organization_id" {
  description = "The organization ID."
  type        = string
}

variable "project_id" {
  description = "The ID of the GCP project."
  type        = string
}

variable "region" {
  description = "The GCP region to create resources in."
  type        = string
  default     = "us-east1"
}

variable "zone" {
  description = "The GCP zone to create resources in."
  type        = string
  default     = "us-east1-b"
}

variable "source_ips" {
  description = "List of IPs to allow connections from"
  type        = list
  default     = []
}

variable "ip_range_pods" {
  description = "The secondary ip range to use for pods"
  default     = "pods"
}

variable "ip_range_services" {
  description = "The secondary ip range to use for services"
  default     = "services"
}